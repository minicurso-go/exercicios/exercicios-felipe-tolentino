package main

import("fmt")

func soma (a float32, b float32) float32{
	return a+b
}

func main(){
	var x,y float32

	fmt.Println("--- Insira dois números para fazer a soma dos dois ---")
	fmt.Print("Número 1: ")
	fmt.Scan(&x)
	fmt.Print("Número 2: ")
	fmt.Scan(&y)

	fmt.Println("Sua soma: ", soma(x,y))
}