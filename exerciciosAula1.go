package main

import "fmt"

func main() {
	// ---  Exercício 1  ---
	/*
	   var nome string
	   var idade int
	   var peso float32

	   fmt.Print("Insira seu nome: ")
	   fmt.Scan(&nome)

	   fmt.Print("Insira sua idade: ")
	   fmt.Scan(&idade)

	   fmt.Print("Insira seu peso: ")
	   fmt.Scan(&peso)

	   fmt.Println("Seu nome é",nome, "/ Sua idade é",idade, "/ Seu peso é",peso)
	*/
	// ---  Exercício 2  ---
	/*
	   var base float32
	   var altura float32

	   fmt.Println("-- Cálcule a área de um retângulo --")

	   fmt.Print("Base: ")
	   fmt.Scan(&base)

	   fmt.Print("Altura: ")
	   fmt.Scan(&altura)

	   fmt.Println("Área do retângulo com base", base, "e altura", altura, "é",base*altura)
	*/
	// ---  Exercício 3  ---
	/*
	   var base float32
	   var altura float32
	   var profundidade float32

	   fmt.Println("-- Cálcule o volume de um retângulo --")

	   fmt.Print("Base: ")
	   fmt.Scan(&base)

	   fmt.Print("Altura: ")
	   fmt.Scan(&altura)

	   fmt.Print("Profundidade: ")
	   fmt.Scan(&profundidade)

	   fmt.Println("Volume do retângulo com base", base, ", altura", altura, "e profundidade", profundidade, "é", base*altura*profundidade)
	*/
	// ---  Exercício 4  ---
	/*
	   var num1 float32
	   var num2 float32
	   var num3 float32
	   var num4 float32

	   fmt.Println("--- Calcule a média aritmética de 4 números ---")

	   fmt.Print("Primeiro número: ")
	   fmt.Scan(&num1)

	   fmt.Print("Segundo número: ")
	   fmt.Scan(&num2)

	   fmt.Print("Terceiro número: ")
	   fmt.Scan(&num3)

	   fmt.Print("Quarto número: ")
	   fmt.Scan(&num4)

	   fmt.Println("A média aritmética dos quatro números é", (num1+num2+num3+num4)/4)
	*/
	// ---  Exercício 5  ---
	/*
	   var valorDolar float32
	   var valorReal float32

	   fmt.Print("Insira um valor em dolar: ")
	   fmt.Scan(&valorDolar)
	   valorDolar *= 5.06

	   fmt.Print("Insira um valor em reais: ")
	   fmt.Scan(&valorReal)
	   valorReal *= 0.20

	   fmt.Println("A transformação de Dolar -> Real ficou:",valorDolar, "/ e a transformação de Real -> Dolar ficou:", valorReal)
	*/
}