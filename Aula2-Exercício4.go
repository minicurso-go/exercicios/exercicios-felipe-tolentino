package main

import("fmt")

func main(){
	var num1 float32
	var num2 float32
	var num3 float32
	var num4 float32

	fmt.Println("--- Escreva quatro números para aplicálos na fórmula (A * B - C * D) ---")
	fmt.Print("Primeiro número: ")
	fmt.Scan(&num1)
	fmt.Print("Segundo número: ")
	fmt.Scan(&num2)
	fmt.Print("Terceiro número: ")
	fmt.Scan(&num3)
	fmt.Print("Quarto número: ")
	fmt.Scan(&num4)

	fmt.Print("Resultado da fórmula (A * B - C * D):",(num1*num2)-(num3*num4))
}