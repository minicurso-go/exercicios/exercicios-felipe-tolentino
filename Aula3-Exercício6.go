package main

import("fmt")

func main(){
    var num int
    numAnterior := 0
    fmt.Println("--- Insira vários números e insira '0' para terminar ---")
    for i:= 0 ;  ; i++ {
        fmt.Print("Número (Digite '0' para sair): ")
        fmt.Scan(&num)
        if num == 0 {
            break
        } else if num > numAnterior {
            numAnterior = num
        }
    }
    fmt.Print("O maior numero digitado foi: ", numAnterior)
}