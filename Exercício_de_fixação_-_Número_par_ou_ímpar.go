package main

import ("fmt")

func main() {
	var num int

	fmt.Println("--- Insira um número inteiro para conferir se é par ou ímpar ---")
	fmt.Print("Número: ")
	fmt.Scan(&num)

	if num%2 == 0 {
		fmt.Print("Seu número é par!")
	} else {
		fmt.Print("Seu número é ímpar!")
	}
}