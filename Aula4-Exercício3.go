package main

import("fmt")

func media (total float32, contador float32) float32 {
	return total/contador
}

func main() {
	var soma, num, count float32

	fmt.Println("--- Digite vários números para fazer a média dos mesmos. (Digite '0' para parar de digitar) ---")

	for i:= 1 ; ; i++ {
		fmt.Print(i, "º número: ")
		fmt.Scan(&num)
		if num != 0 {
			soma += num
			count++
		} else {
			break
		}
	}
	fmt.Print("Sua média ficou: ", media(soma,count))
}