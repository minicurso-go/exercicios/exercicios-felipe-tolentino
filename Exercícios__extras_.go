/* 1
package main

import "fmt"

func main() {
  var x [3]int
  var soma int
  fmt.Println("Insira 3 valores")
  for i := 0 ; i < 3 ; i++ {
      fmt.Print("Valor ", i+1, ": ")
      fmt.Scan(&x[i])
      soma += x[i]
  }
  fmt.Print("A soma dos valores é: ",soma)
}
*/
/* 2
package main

import "fmt"

func main() {
  x := []int{1,2,3,4,5}
  x = append(x[:2], x[3:]...)
  
  fmt.Print(x)
  
}
*/
/* 3
package main

import "fmt"

func main() {
  var x [4]int
  produto := 1
  fmt.Println("Insira 5 valores")
  for i := 0 ; i < 4 ; i++ {
      fmt.Print("Valor ", i+1, ": ")
      fmt.Scan(&x[i])
      produto *= x[i]
  }
  fmt.Print("A soma dos valores é: ",produto)
}
*/
/* 4
package main

import "fmt"

func main() {
  var tamanho int
  fmt.Print("Insira o tamanho do seu slice: ")
  fmt.Scan(&tamanho)
  x := make([]int,tamanho)
  fmt.Println("Insira os valores no seu slice de tamanho ",tamanho)
  for i := 0 ; i < tamanho ; i++ {
      fmt.Print("Valor ", i+1, ": ")
      fmt.Scan(&x[i])
  }
  fmt.Print("O seu slice ficou assim: ",x)
  
}
*/
/* 5
package main

import "fmt"

func main() {
  x := [10]int{8,1,5,9,3,6,4,10,7,2}
  var a,b int
  fmt.Println("Insira um valora para descobrir se está no Array: ")
  fmt.Scan(&a)
  for i := 0 ; i < 10 ; i++ {
      if a == x[i] {
          fmt.Print("O valor ", a, " inserido estava na " , i+1, "ª posição no índice ", i)
          b++
          break
        }
      }
      if b != 1 {
          fmt.Print("O valor inserido não foi encontrado")
      }
}
*/
//-------------------------------------------------------------------------------------------------//
// Exercícios que o Kaynan fez no quadro
/*
package main

import("fmt")

func main() {
    var palavra string
    
    fmt.Println("Digite uma palavra")
    fmt.Scan(&palavra)
    
    inicio := 0
    final:= len(palavra) - 1
    flag := false
    
    for final > inicio {
        if palavra[inicio] != palavra[final] {
            flag = false
            break
        } else {
            flag = true
        }
        final--
        inicio++
    }
    if flag == true {
        fmt.Print("Palíndromo!")
    } else {
        fmt.Print("Não palíndromo!")
    }
}
*/
/*
package main

import("fmt")

func main() {
    var numero int
    
    fmt.Print("Digite um número: ")
    fmt.Scan(&numero)
    
    soma:=0
    
    for i := 1; i < numero ; i++ {
        if numero%i == 0 {
            soma += i
        }
    }
    if numero == soma {
        fmt.Print("Numero Perfeito!")
    } else {
        fmt.Print("Numero normal")
    }
}
*/
/*
package main

import("fmt")

func main() {
    var numeros []int
    
    for true {
        var numero int
        fmt.Print("Digite um número (Digite 0 para sair): ")
        fmt.Scan(&numero)
        
        if numero == 0 {
            break
        } else {
            numeros = append(numeros, numero)
        }
    }
    
    fmt.Println("Lista: ",numeros)
    numeroMaior := -1
    
    for i := 0 ; i < numero ; i++ {
        if numeros[i] > numeroMaior {
            numeroMaior = numeros[i]
        }
    }
    
    fmt.Println("Numero Maior ", numeroMaior)
    
    segundoMaior := -1
    for i := 0 ; i < len(numeros) ; i++ {
        if numeros[i] < numeroMaior {
            segundoMaior = numeros[i]
        }
    } 
    fmt.Println("Segundo Numero Maior ", segundoMaior)
    
}
*/
/*
package main

import("fmt")

func main() {
    var palavra string
    
    fmt.Print("Digite uma palavra: ")
    fmt.Scan(&palavra)
    
    for i := 0 ; i < len(palavra) ; i++ {
        fmt.Printf("%c\n", palavra[i])
    } 
}
*/
/*
package main

import("fmt")

func main(){
    var inicio, final, alvo int
    
    fmt.Print("Digite o início: ")
    fmt.Scan(&inicio)
    
    fmt.Print("Digite o final: ")
    fmt.Scan(&final)
    
    fmt.Print("Digite o valor alvo: ")
    fmt.Scan(&alvo)
    
    for i := inicio; i <= final ; i++ {
        for j := 0 ; j <= final ; j++ {
            if i!=j {
                soma := 0
                soma += i+j
                
                if soma == alvo {
                    fmt.Print("Valores: ", i, " e ", j)
                }
            }
        }
    }
}
*/
/*
package main

import("fmt")

func primo (p int) int {
	var count int
	for i := p ; i>0 ; i-- {
		if p % i == 0 {
			count++
		}
	}
	return count
}

func main() {
    var num int
    var count int
    
    fmt.Println("Insira um número para quantos primos existem até ele: ")
    fmt.Print("Numero: ")
    fmt.Scan(&num)
    
    for i := 1 ; i <= num ; i++ {
        if primo(i) == 2 {
            count++
        }
    }
    fmt.Print("Foram ", count, " até o número solicitado (", num, ")")
}
*/
// Crie uma matriz bidimensional de inteiros com 3 linhas e 4 colunas. Inicialize cada elemento com o valor da soma do índice da linha e o índice da coluna. Imprima a matriz resultante.
/*
package main

import "fmt"

func main() {
    var matriz [3][4]int

    for i := 0 ; i<3 ; i++ {
        for j := 0 ; j<4 ; j++ {
            matriz[i][j] = i+j
        }
    }
    for i := 0 ; i<3 ; i++ {
        for j := 0 ; j<4 ; j++ {
            fmt.Print("[", i, "][", j,"] =", matriz[i][j], " / ")
        }
        fmt.Printf("\n")
    }
}
*/