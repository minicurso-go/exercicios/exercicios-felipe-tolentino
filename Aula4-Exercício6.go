package main

import("fmt")

func potencia (p, j int) int {
	pAnterior := p
	for i := 1 ; i < j ; i++ {
		pAnterior *= p
	}
	return pAnterior
}

func main() {
	var x, y int

	fmt.Println("--- Insira um valor para para determinar um número e um para determinar a potência desse número ---")
	fmt.Print("Número: ")
	fmt.Scan(&x)
	fmt.Print("Potência: ")
	fmt.Scan(&y)
	fmt.Print(x, " elevado à ", y, " é ", potencia(x,y))
}