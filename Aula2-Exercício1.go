package main

import (
	"fmt"
)

func main() {
	var num1 float32
	var num2 float32

	fmt.Println("--- Escreva dois valores de um plano cartesiano ---")
	fmt.Print("Primeiro Número (x): ")
	fmt.Scan(&num1)
	fmt.Print("Segundo Número (y): ")
	fmt.Scan(&num2)

	if num1 == 0 && num2 ==0 {
		fmt.Println("Você está na origem! (0,0)")
	} else if num1 == 0 {
	    fmt.Println("Você está no eixo das ordenadas! (Eixo X)")
	} else if num2 == 0 {
	    fmt.Println("Você está no eixo das abscissas! (Eixo Y)")
	} else if num1 > 0 && num2 > 0 {
	    fmt.Println("Você está no primeiro quadrante!")
    } else if num1 < 0 && num2 > 0 {
	    fmt.Println("Você está no segundo quadrante!")
	} else if num1 < 0 && num2 < 0 {
	    fmt.Println("Você está no terceiro quadrante!")
	} else if num1 > 0 && num2 < 0 {
	    fmt.Println("Você está no quarto quadrante!")
	}
    fmt.Print("Coordenadas atuais: (", num1, "/",num2, ")")
}