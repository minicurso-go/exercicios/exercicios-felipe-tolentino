// OBS: (Na minha opinião) Não seria possível fazer esses exercícios sem o uso de um Array, que não vimos na aula 4, mas como a aula 5 já esta postada, e é sobre arrays, foi possível fazê-lo.
package main

import("fmt")

func procura (a int) int {
	var count int
	var lista = [10]int{3,6,9,2,4,1,5,7,10,8}
	for i := 0 ; i<10 ; i++ {
		if a != lista[i] {
			count++
		} else {
			break
		}
	}
	return count
}

func main() {
	var num int

	fmt.Println("--- Digite um número de 1 a 10 para procurar o seu índice em determinada lista ---")
	fmt.Print("Número: ")
	fmt.Scan(&num)
	fmt.Print("O número ", num, " foi encontrado no índice ", procura(num), " da lista (na ", procura(num)+1, "ª posição)")
}