package main

import ("fmt")

func main() {
	var num1 float32
	var num2 float32
	var op int

	fmt.Println("--- Escreva dois valores e a operação que deseja fazer entre os dois ---")
	fmt.Print("Primeiro Número: ")
	fmt.Scan(&num1)
	fmt.Print("Segundo Número: ")
	fmt.Scan(&num2)
	fmt.Printf("Escreva um número de 1 a 4\n1 - Soma\n2 - Subtração\n3 - Multiplicação\n4 - Divisão\nEscolha: ")
	fmt.Scan(&op)

	if op == 1 {
	    fmt.Print("Soma = ",num1+num2)
	} else if op == 2 {
	    fmt.Print("Subtração = ",num1-num2)
	} else if op == 3 {
	    fmt.Print("Multiplicação = ",num1*num2)
	} else {
	    fmt.Print("Divisão = ",num1/num2)
	}
}