package main

import("fmt")

func main(){
	var a float64
	var b float64
	var c float64

	fmt.Println("--- Escreva três números para descobrir operações relacionadas a eles ---")
	fmt.Print("Primeiro número: ")
	fmt.Scan(&a)
	fmt.Print("Segundo número: ")
	fmt.Scan(&b)
	fmt.Print("Terceiro número: ")
	fmt.Scan(&c)


	fmt.Println("A área de um triangulo retângulo de base 'a' e altura 'c' é:",(a*b)/2)
	fmt.Println("A área de um círculo de raio 'c' é:",(c*c)*3.14159)
	fmt.Println("A área de um trapézio que tem 'a' e 'b' por bases e 'c' como altura : ",((a+b)*c)/2)
	fmt.Println("A área de um quadrado que tem lado 'b' é:",b*b)
	fmt.Println("A área de um retângulo que tem lados 'a' e 'b' é:",a*b)
}