package main

import("fmt")

func primo (p int) int {
	var count int
	for i := p ; i>0 ; i-- {
		if p % i == 0 {
			count++
		}
	}
	return count
}

func main() {
	var num int

	fmt.Println("--- Insira um número para descobrir se ele é primo ---")
	fmt.Print("Número: ")
	fmt.Scan(&num)

	if primo(num) == 2 {
		fmt.Print("O número ", num, " é primo")
	} else {
		fmt.Print("O número ", num, " NÃO é primo")
	}
}