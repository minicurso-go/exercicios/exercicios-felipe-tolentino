package main

import("fmt")

func fatorial (j int) int {
	f := 1
	for i := 1 ; i<=j ; i++ {
		f*=i
	}
	return f
}

func main() {
	var x int

	fmt.Println("--- Insira um valor para descobrir o seu fatorial ---")
	fmt.Print("Número: ")
	fmt.Scan(&x)
	fmt.Print(x,"! é igual a: ",fatorial(x))
}