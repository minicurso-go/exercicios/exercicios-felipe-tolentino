package main

import("fmt")

func main(){
    var num int
    
    fmt.Println("--- Insira um número para ver sua tabuada ---")
    fmt.Print("Número: ")
    fmt.Scan(&num)
    
    for i:= 1 ; i<=10 ; i++ {
        fmt.Println(num, "*", i, "=", num*i)
    }
}