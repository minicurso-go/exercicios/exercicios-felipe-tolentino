package main

import("fmt")

func CelFah (a float32) float32 {
	// Cº para Fº: (0 °C × 9/5) + 32 = 32 °F
	return (a * 9/5) + 32
}

func FahCel (a float32) float32 {
	// Fº para Cº: (0 °F − 32) × 5/9 = -17,78 °C
	return (a - 32) * 5/9
}

func main(){
	var x,y float32

	fmt.Println("--- Conversão de Celsius para Fahrenheit ou vice-versa ---")
	fmt.Printf("Escolha qual conversão você gostaria de fazer:\n1- Celsius para Fahrenheit\n2- Fahrenheit para Celsius\nEscolha: ")
	fmt.Scan(&y)
	if y == 1 {
		fmt.Print("Escolha um valor: ")
		fmt.Scan(&x)
		fmt.Print("Sua conversão: ", CelFah(x), " Fº")
	} else if y == 2 {
		fmt.Print("Escolha um valor: ")
		fmt.Scan(&x)
		fmt.Print("Sua conversão: ", FahCel(x), " Cº")
	} else {
		fmt.Print("Valor Inválido")
	}
}